# splyt-angular-app
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.3.

##### Project basics
- Clone project using https://gitlab.com/jetjet.dev/splyt-angular-app.git 
- Checkout `main` branch

##### Steps to serve http://localhost:4200/
1. Run `npm i` to install dependencies and required files.
2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

##### Building the project
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
