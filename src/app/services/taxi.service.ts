import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TaxiDriverList } from '../models/taxi';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaxiService {
  constructor(private http: HttpClient) { }
  
  getTaxiDrivers(params: GetTaxiDriversParams): Observable<TaxiDriverList> {
    let url = `${environment.apiHost}/taxi/drivers`;

    if (params.lat) url += `/${params.lat}`;
    if (params.lng) url += `/${params.lng}`;
    if (params.count) url += `/${params.count}`;

    return this.http.get<TaxiDriverList>(url).pipe(
      catchError(err => {
        console.log(err);
        return throwError(new Error('Unable to get Taxi at this moment.'));
      })
    );
  }  
}

export interface GetTaxiDriversParams {
  lat?: number;
  lng?: number;
  count?: number;
}
