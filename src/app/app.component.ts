import { MapsAPILoader } from '@agm/core';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of, Subject, timer } from 'rxjs';
import { debounceTime, delay, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { Driver } from './models/taxi';
import { GetTaxiDriversParams, TaxiService } from './services/taxi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  TAXI_ICON = {
    url: '../assets/taxi.png',
    scaledSize: {
      width: 25,
      height: 25
    }
  };
  POLLING_INTERVAL = 15000;
  private destroyed$ = new Subject();
  private slider$ = new Subject<number>();
  private polling$ = new Subject();
  title = 'splyt-angular-app';
  offices = [
    {
      name: 'Singapore Office',
      coords: {
        lat: 1.285194,
        lng: 103.8522982
      },
      nearest: false
    },
    {
      name: 'London Office',
      coords: {
        lat: 51.5049375,
        lng: -0.0964509
      },
      nearest: false
    }
  ]
  lat = 1.285194;
  lng = 103.8522982;
  count = 5;
  officeTitle = '';
  drivers: Driver[] = [];
  constructor(
    private snackbar: MatSnackBar,
    private mapLoader: MapsAPILoader,
    private taxiService: TaxiService
  ) { }

  ngOnInit(): void {
    this.locateUser();

    // Subscribe to slider input values 
    // debouceTime helps to slow down frequent inputs
    this.slider$.pipe(
      debounceTime(500)
    ).subscribe((value: number) => {
      this.count = value;
      this.loadTaxi(value);
    });

    // Polling every 15 seconds
    this.polling$.pipe(
      startWith(0),
      delay(this.POLLING_INTERVAL),
      switchMap(() => timer(0, this.POLLING_INTERVAL)),
      takeUntil(this.destroyed$),
      switchMap(() => {
        this.loadTaxi(this.count);
        return of(true);
      })
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  locateUser(): void {
    // mapLoader.load() callback ensures the logic runs after google map js files are fully loaded 
    this.mapLoader.load().then(() => {
      if (navigator.geolocation) {
        // Gets user current location
        navigator.geolocation.getCurrentPosition((position) => {
          const { coords } = position;
          this.getUserNearestOffice(coords);
        });
      } else {
        this.snackbar.open('Geolocation is not supported by this browser.', 'Okay', {
          panelClass: 'snackbar-warning'
        });
      }
    });
  }

  getUserNearestOffice(coords: GeolocationCoordinates): void {
    const distances = [];
    let closest = -1;
    const userPosition = new google.maps.LatLng(coords.latitude, coords.longitude);
    // const userPosition = new google.maps.LatLng(41.017104875567675, 28.97992294284658); // Random location close to London office

    // Loop offices to find the nearest office to user's current location
    for (let i = 0; i < this.offices.length; i++) {
      const office = this.offices[i];
      const d = google.maps.geometry.spherical.computeDistanceBetween(
        userPosition,
        new google.maps.LatLng(office.coords.lat, office.coords.lng)
      );

      distances[i] = d;
      if (closest == -1 || d < distances[closest]) {
        closest = i;
      }
    }

    if (closest > -1) {
      // Set map default view to closest office
      this.setMapDefaultView(closest);
      this.offices[closest].nearest = true;

      // Load taxi on the map
      this.loadTaxi();
      // this.polling$.subscribe();

      // Display info
      this.snackbar.open(`Closest marker is ${this.offices[closest].name}`, 'Okay', {
        panelClass: 'snackbar-info',
        horizontalPosition: 'right',
        verticalPosition: 'bottom'
      });
    } else {
      this.snackbar.open('Unable to determine closest Splyt office.', 'Okay', {
        panelClass: 'snackbar-warning',
        horizontalPosition: 'right',
        verticalPosition: 'bottom'
      });
    }
  }

  loadTaxi(count: number = 5): void {
    const params: GetTaxiDriversParams = {
      lat: this.lat,
      lng: this.lng,
      count
    }

    this.taxiService.getTaxiDrivers(params).pipe(
      takeUntil(this.destroyed$)
    ).subscribe(
        res => {
          this.drivers = res.drivers;
        },
        (err: string) => {
          this.snackbar.open(err, 'Okay', {
            panelClass: 'snackbar-error',
            horizontalPosition: 'right',
            verticalPosition: 'bottom'
          });
        });
  }
  
  officeSelected(index: number): void {
    this.setMapDefaultView(index);
  }

  nearestSelected(): void {
    this.setMapDefaultView();
  }

  formatedText(value: number): string | number {
    if (value === 20) return 'MAX';
    else return value;
  }

  sliderChanged(change: MatSliderChange): void {
    if (change.value && change.value >= 0) {
      this.slider$.next(change.value);
    }
  }

  private setMapDefaultView(index: number = -1) {
    // Set map default view to closest office
    if (index < 0) {
      index = this.offices.findIndex(o => o.nearest);
    }

    this.lat = this.offices[index].coords.lat;
    this.lng = this.offices[index].coords.lng;
    this.officeTitle = this.offices[index].name;
    this.loadTaxi(this.count);
  }
}
