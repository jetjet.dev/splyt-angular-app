import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule } from '@angular/material/table'
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import {MatSliderModule} from '@angular/material/slider';
@NgModule({
  imports: [
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatMenuModule,
    MatSliderModule
  ],
  exports: [
    MatToolbarModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatMenuModule,
    MatSliderModule
  ]
})
export class MaterialModule { }

